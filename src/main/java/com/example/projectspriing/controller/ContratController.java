package com.example.projectspriing.controller;


import com.example.projectspriing.entity.Contrat;
import com.example.projectspriing.service.IContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;


import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/contrat")
public class ContratController {

    @Autowired
    @Qualifier(value = "contratService")
    IContractService cs;
    
        @Autowired
        IContractService contractService;


        // http://localhost:8089/SpringMVC/contrat/getAll
        @GetMapping("/getAll")
        @ResponseBody
        public List<Contrat> getAll() {
            return contractService.findAll();
        }


        // http://localhost:8089/SpringMVC/contrat/nbContratsValides
        @GetMapping("/nbContratsValides")
        @ResponseBody
        public Integer nbContratsValides(
                @RequestParam(value = "fromDate")@DateTimeFormat(pattern = "yyyy-MM-dd") Date fromDate,
                @RequestParam(value = "toDate")@DateTimeFormat(pattern = "yyyy-MM-dd") Date toDate
        ) {
            return contractService.nbContratsValides(fromDate, toDate);
        }
        // http://localhost:8089/SpringMVC/contrat/add
        @PostMapping("/add")
        @ResponseBody
        public void add(
                @RequestParam(value = "fromDate")@DateTimeFormat(pattern = "yyyy-MM-dd") Date fromDate,
                @RequestParam(value = "toDate")@DateTimeFormat(pattern = "yyyy-MM-dd") Date toDate
        ) {

        }
    
}
