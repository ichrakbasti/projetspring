package com.example.projectspriing.controller;


import com.example.projectspriing.service.IEtudiantService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Configuration
@EnableScheduling
@RestController
@Slf4j
@RequestMapping("/etudiants")
public class EtudiantController {

    @Autowired
    @Qualifier(value = "etudiantService")
    IEtudiantService es;

    // http://localhost:8089/SpringMVC/etudiants/nbEtudiant
    @GetMapping("/nbEtudiant")
    @ResponseBody
    @Scheduled(fixedDelay = 180000)
    public void nbEtudiantPerOption() {
        log.info(es.nbEtudiantPerOption().toString());
    }


}
