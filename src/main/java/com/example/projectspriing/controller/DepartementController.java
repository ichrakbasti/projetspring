package com.example.projectspriing.controller;


import com.example.projectspriing.service.IDepartementService;
import com.example.projectspriing.service.IEtudiantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DepartementController {

    @Autowired
    @Qualifier(value = "departementService")
    IDepartementService ds;
}
