package com.example.projectspriing.repository;

import com.example.projectspriing.entity.Etudiant;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface EtudiantRepository extends CrudRepository<Etudiant, Long> {

    @Query("SELECT count(e) FROM Etudiant e GROUP BY e.op")
    List<Integer> countEtudiant();
}
