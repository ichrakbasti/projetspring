package com.example.projectspriing.repository;

import com.example.projectspriing.entity.DetailEquipe;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DetailEquipeRepository extends CrudRepository<DetailEquipe, Long> {
}
