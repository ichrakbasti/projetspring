package com.example.projectspriing.repository;

import com.example.projectspriing.entity.Universite;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UniversiteRepository extends CrudRepository<Universite, Long> {
}
