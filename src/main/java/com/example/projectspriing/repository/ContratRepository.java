package com.example.projectspriing.repository;

import com.example.projectspriing.entity.Contrat;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


@Repository
public interface ContratRepository extends CrudRepository<Contrat, Long> {

    @Query(value = "", nativeQuery = true)
    Contrat findByDateFinContratBetweenOrDateDebutContratGreaterThan(Date d, Date d2, Date d3);
    /*
@Modifying
   @Query(value="Update entreprise  set adresse = : adresse where idEntreprise = :idEntreprise;", nativeQuery = true)
   int updateEntrepriseByAdresse(String adresse, int id)
 */
//    contret li id equipe 7
//    wdelzte contrat where universite = 3

    List<Contrat> findContratsByEtudiant_EquipesIdEquipe(Long idEquipe);

    @Query(value = "delete contrat c where idEtudiant", nativeQuery = true)
    void  deleteContratByEtudiantDepartementIdUniversite(int idUniversite );

    Integer countByDateDebutContratGreaterThanAndDateFinContratLessThanAndArchiveFalse(Date dateDebut, Date dateFin);
}
