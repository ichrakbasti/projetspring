package com.example.projectspriing.repository;

import com.example.projectspriing.entity.Departement;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartementRepository extends CrudRepository<Departement, Long> {
}
