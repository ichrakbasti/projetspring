package com.example.projectspriing.repository;

import com.example.projectspriing.entity.Equipe;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface EquipeRepository extends CrudRepository<Equipe, Long> {

}
