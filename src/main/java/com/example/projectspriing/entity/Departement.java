package com.example.projectspriing.entity;

import lombok.*;
import netscape.security.UserTarget;

import javax.persistence.*;
import java.util.Set;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@EqualsAndHashCode
public class Departement {
    @Id
    private long idDepart;
    private String nomDepart;

    @OneToMany(cascade = CascadeType.ALL, mappedBy="departement")
    private Set<Etudiant> etudiants;

}
