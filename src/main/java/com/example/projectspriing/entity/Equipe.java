package com.example.projectspriing.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import java.util.List;
import java.util.Set;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@EqualsAndHashCode
public class Equipe {
    @Id
    private long idEquipe;
    private String nomEquipe;
    @ManyToMany
    List<Etudiant> etudiants;

    @OneToOne
    private DetailEquipe detailEquipe;
}
