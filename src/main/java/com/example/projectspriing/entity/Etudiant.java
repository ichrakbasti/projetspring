package com.example.projectspriing.entity;


import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@EqualsAndHashCode
public class Etudiant {
    @Id
    private long idEtudiant;

    private String prenomE;

    private  String nomE;
    @ManyToOne
    private Departement departement;

    @OneToMany(mappedBy = "etudiant", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Contrat> contrats;

    @ManyToMany( mappedBy = "etudiants",cascade = CascadeType.ALL)
    Set<Equipe>equipes;


    @Enumerated(EnumType.STRING)

    private Option op;




}
