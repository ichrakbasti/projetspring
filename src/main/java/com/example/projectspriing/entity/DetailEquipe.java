package com.example.projectspriing.entity;

import lombok.*;

import javax.persistence.*;


@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@EqualsAndHashCode
public class DetailEquipe {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    private long salle ;
    private String thematique;


    @OneToOne(mappedBy = "detailEquipe")
    private  Equipe equipes;
}
