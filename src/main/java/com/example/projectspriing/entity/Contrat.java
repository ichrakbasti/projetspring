package com.example.projectspriing.entity;


import lombok.*;

import javax.persistence.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@EqualsAndHashCode
public class Contrat {
    @Id
    private long idContrat;
    @Temporal(TemporalType.DATE)
    private java.util.Date dateDebutContrat;

    private  java.util.Date dateFinContrat;

    private boolean archive;

    @ManyToOne
    private Etudiant etudiant;

}
