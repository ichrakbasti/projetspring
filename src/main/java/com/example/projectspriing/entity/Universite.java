package com.example.projectspriing.entity;

import lombok.*;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Set;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@EqualsAndHashCode
public class Universite {
    @Id
    private  long idUniv;
    private Long nomUniv;

    @OneToMany

    private Set<Departement> departements;
}