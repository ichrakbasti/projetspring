package com.example.projectspriing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectSpriingApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectSpriingApplication.class, args);
    }

}
