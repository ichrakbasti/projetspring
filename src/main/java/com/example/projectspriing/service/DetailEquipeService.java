package com.example.projectspriing.service;

import com.example.projectspriing.entity.DetailEquipe;
import com.example.projectspriing.repository.DetailEquipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DetailEquipeService implements IDetailEquipeService{

    @Autowired
    DetailEquipeRepository detailEquipeRepository;

    long create (DetailEquipe d){
        return detailEquipeRepository.save(d).getId();
    }

    boolean delete (DetailEquipe d){
        if (detailEquipeRepository.existsById(d.getId()) ){
            detailEquipeRepository.delete(d);
            return true;
        }
        return false;

    }

    boolean update (DetailEquipe d){
        if (detailEquipeRepository.existsById(d.getId()) ){
            detailEquipeRepository.save(d);
            return true;
        }

        return false;
    }
    List<DetailEquipe> findAll (){
        return (List<DetailEquipe>) detailEquipeRepository.findAll();
    }

    DetailEquipe findById (long id) {
        return detailEquipeRepository.findById(id).orElse(null);
    }

}
