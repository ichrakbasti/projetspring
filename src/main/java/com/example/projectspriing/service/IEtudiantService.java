package com.example.projectspriing.service;

import com.example.projectspriing.entity.Etudiant;
import com.example.projectspriing.repository.EtudiantRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public interface IEtudiantService {

    long create (Etudiant e);

    boolean delete (Etudiant e);

    boolean update (Etudiant e);
    List<Etudiant> findAll ();

    Etudiant findById (long id);

    List<Integer> nbEtudiantPerOption();
}
