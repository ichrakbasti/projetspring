package com.example.projectspriing.service;

import com.example.projectspriing.entity.Equipe;

import java.util.List;

public interface IEquipeService {


    long create(Equipe e);

    boolean delete(Equipe e);

    boolean update(Equipe e);

    List<Equipe> findAll();

    Equipe findById(long id) ;

    void assignEtudiantToEquipe(Integer idEtudiant, Integer idEquipe);
}
