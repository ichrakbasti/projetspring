package com.example.projectspriing.service;

import com.example.projectspriing.entity.Etudiant;
import com.example.projectspriing.repository.EtudiantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class EtudiantService implements IEtudiantService{

    @Autowired
    EtudiantRepository etudiantRepository;

    public long create (Etudiant e){
        return etudiantRepository.save(e).getIdEtudiant();
    }

    public boolean delete (Etudiant e){
        if (etudiantRepository.existsById(e.getIdEtudiant()) ){
            etudiantRepository.delete(e);
            return true;
        }
        return false;

    }

    public boolean update (Etudiant e){
        if (etudiantRepository.existsById(e.getIdEtudiant()) ){
            etudiantRepository.save(e);
            return true;
        }

        return false;
    }
    public List<Etudiant> findAll (){
        return (List<Etudiant>) etudiantRepository.findAll();
    }

    public Etudiant findById (long id) {
        return etudiantRepository.findById(id).orElse(null);
    }

    public List<Integer> nbEtudiantPerOption() {

        return etudiantRepository.countEtudiant();
    }
}
