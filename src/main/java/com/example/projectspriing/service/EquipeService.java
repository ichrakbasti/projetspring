package com.example.projectspriing.service;

import com.example.projectspriing.entity.Equipe;
import com.example.projectspriing.entity.Etudiant;
import com.example.projectspriing.repository.EquipeRepository;
import com.example.projectspriing.repository.EtudiantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EquipeService implements IEquipeService {

    @Autowired
    EquipeRepository equipeRepository;

    @Autowired
    EtudiantService etudiantService;

    public long create(Equipe e) {
        return equipeRepository.save(e).getIdEquipe();
    }

    public boolean delete(Equipe e) {
        if (equipeRepository.existsById(e.getIdEquipe())) {
            equipeRepository.delete(e);
            return true;
        }
        return false;

    }

    public boolean update(Equipe e) {
        if (equipeRepository.existsById(e.getIdEquipe())) {
            equipeRepository.save(e);
            return true;
        }

        return false;
    }

    public List<Equipe> findAll() {
        return (List<Equipe>) equipeRepository.findAll();
    }

    public Equipe findById(long id) {
        return equipeRepository.findById(id).orElse(null);
    }

    public void assignEtudiantToEquipe(Integer idEtudiant, Integer idEquipe){

        Equipe equipe = this.findById(idEquipe);
        Etudiant etudiant = etudiantService.findById(idEtudiant);
        equipe.getEtudiants().add(etudiant);

        equipeRepository.save(equipe);

    }
}
