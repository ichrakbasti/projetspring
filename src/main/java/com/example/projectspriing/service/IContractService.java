package com.example.projectspriing.service;

import com.example.projectspriing.entity.Contrat;

import java.util.Date;
import java.util.List;

public interface IContractService {
    void x();

    long create (Contrat c);

    boolean delete (Contrat c);

    boolean update (Contrat c);
    List<Contrat> findAll ();

    Contrat findById (long id);
    Integer nbContratsValides (Date startDate, Date endDate);

}
