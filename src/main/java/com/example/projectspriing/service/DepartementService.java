package com.example.projectspriing.service;

import com.example.projectspriing.entity.Departement;
import com.example.projectspriing.repository.DepartementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartementService implements IDepartementService{
    @Autowired
    DepartementRepository departementRepository;

    long create (Departement d){
        return departementRepository.save(d).getIdDepart();
    }

    boolean delete (Departement d){
        if (departementRepository.existsById(d.getIdDepart()) ){
            departementRepository.delete(d);
            return true;
        }
        return false;

    }

    boolean update (Departement d){
        if (departementRepository.existsById(d.getIdDepart()) ){
            departementRepository.save(d);
            return true;
        }

        return false;
    }
    List<Departement> findAll (){
        return (List<Departement>) departementRepository.findAll();
    }

    Departement findById (long id) {
        return departementRepository.findById(id).orElse(null);
    }
    
}
