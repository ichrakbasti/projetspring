package com.example.projectspriing.service;

import com.example.projectspriing.entity.Contrat;
import com.example.projectspriing.repository.ContratRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ContratService implements IContractService{

    @Autowired
    ContratRepository contratRepository;

    public void x(){
        contratRepository.findByDateFinContratBetweenOrDateDebutContratGreaterThan(new Date(), new Date(), new Date());
    }

    public long create (Contrat c){



        return contratRepository.save(c).getIdContrat();
    }

    public boolean delete (Contrat c){
        if (contratRepository.existsById(c.getIdContrat()) ){
            contratRepository.delete(c);
            return true;
        }
        return false;

    }

    public boolean update (Contrat c){
        if (contratRepository.existsById(c.getIdContrat()) ){
            contratRepository.save(c);
            return true;
        }

        return false;
    }
    public List<Contrat> findAll (){
        return (List<Contrat>) contratRepository.findAll();
    }

    public Contrat findById (long id) {
        return contratRepository.findById(id).orElse(null);
    }

    public Integer nbContratsValides(Date startDate, Date endDate) {

        return contratRepository.countByDateDebutContratGreaterThanAndDateFinContratLessThanAndArchiveFalse(startDate,endDate);
    }

}
